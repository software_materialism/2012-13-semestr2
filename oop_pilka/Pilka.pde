class Pilka {
  int srednica, x, y, vx, vy;
  boolean blokada = false;

  Pilka() {
    srednica = 30;
    wPoczPred();
    rysuj();
  }

  Pilka(int srednica) {
    this.srednica = srednica;
    wPoczPred();
    rysuj();
  }

  Pilka(int x, int y, int srednica) {
    this.x = x;
    this.y = y;
    this.srednica = srednica;
    wPoczPred();
    rysuj();
  }
  
  void wPoczPred(){
    vx = int(random(-4,4));
    vy = (int)random(-4,4);
  }

  void rysuj() {
    poruszaj();
    ellipse(x, y, srednica, srednica);
  }

  void przeciagnij(int nowyX, int nowyY) {
    boolean blokada = jestZablokowana(nowyX, nowyY);
    if (blokada == false) {
      x = nowyX;
      y = nowyY;
    } 
  }
  
  boolean jestZablokowana(int _x, int _y){
   int dystans = (int)sqrt(sq(x - _x) + sq(y - _y));
   if (dystans <= srednica/2){
     blokada = false;
   } else {
     blokada = true;
   }
   return blokada; 
  }
  
  void poruszaj(){
    x += vx; 
    y += vy;
  }
  
}

