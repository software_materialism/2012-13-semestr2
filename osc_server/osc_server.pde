import oscP5.*;
import netP5.*;


OscP5 oscP5;
NetAddress[] instr = {
  new NetAddress("192.168.24.88", 12000), 
  new NetAddress("192.168.24.24", 12000), 
  new NetAddress("192.168.24.94", 12000), 
  new NetAddress("192.168.24.92", 12000), 
  new NetAddress("192.168.24.95", 12000), 
  new NetAddress("192.168.24.78", 12000),
};

void setup() {
  oscP5 = new OscP5(this, 12000);
}

void draw() {
  OscMessage komunikat = new OscMessage("/uderzenie");
  int fq = int(random(4));
  komunikat.add(fq);
  int numerFali = (int)random(4);
  komunikat.add(numerFali);
  int numerInstr = int(random(instr.length));
  NetAddress _instr = instr[numerInstr];
  oscP5.send(komunikat, _instr);
}

