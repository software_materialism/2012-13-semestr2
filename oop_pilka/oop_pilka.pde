Pilka[] pilki = new Pilka[10];

void setup() {
  size(400, 400);
  for (int i = 0; i < 10; i = i +1) {
    int srednica = (int)random(10, 40);
    int x = (int)random(srednica/2, width-srednica/2);
    int y = (int)random(srednica/2, height-srednica/2);
    pilki[i] = new Pilka(x, y, srednica);
  }
}

void draw() {
  background(200);
  for (int i = 0; i < 10; i++) {
    Pilka pilka = pilki[i];
    int aktualneX = pilka.x;
    int aktualneY = pilka.y;

    if ( (aktualneX > width - pilka.srednica/2) || (aktualneX < 0 + pilka.srednica/2) ) {
      pilka.vx = -pilka.vx;
    }
    if ( (aktualneY > height - pilka.srednica/2) || (aktualneY < 0 + pilka.srednica/2) ) {
      pilka.vy = -pilka.vy;
    }

    pilka.rysuj();
  }
}

void mouseDragged() {
  for (int i = 0; i < 10; i++) {
    pilki[i].przeciagnij(mouseX, mouseY);
  }
}


void mouseReleased() {
  int deltaX = mouseX - pmouseX;
  int deltaY = mouseY - pmouseY;
//  for (int i = 0; i < 10; i++) {
//    if (pilki[i].blokada == false) {
//      pilki[i].vx = deltaX;
//      pilki[i].vy = deltaY;
//    }
//  }
  for(Pilka pilka : pilki){
     if (pilka.blokada == false) {
      pilka.vx = deltaX;
      pilka.vy = deltaY;
    }
  }
}


