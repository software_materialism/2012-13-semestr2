import beads.*;
import oscP5.*;
import netP5.*;


AudioContext ac;
Gain g;
Envelope env;
WavePlayer wp;
Buffer[] fale = {
  Buffer.SINE, Buffer.SQUARE, Buffer.SAW, Buffer.TRIANGLE
};

OscP5 oscP5;

float[] akord  = {
  60, 64, 67, 69
};

float bcolor = 0;
int r = 300;
int newR = 0;

void setup() {
  size(400, 400);

  ac = new AudioContext(); 
  for (int i = 0; i<akord.length; i++) {
    akord[i] = Pitch.mtof(akord[i]);
  }

  env= new Envelope(ac, 0);
  g = new Gain(ac, 1, env);
  wp = new WavePlayer(ac, akord[(int)random(4)], Buffer.SINE);
  ///////////////////////////////////////////
  g.addInput(wp);
  ac.out.addInput(g);
  ac.start();
  
  oscP5 = new OscP5(this, 12000);
}

void draw() {
  background(bcolor);
  //   r = env.getCurrentValue()*200;
  r = easing(r, newR, 0.5);
  if (r == newR) newR = 0;
  ellipse(200, 200, r, r);
}


int easing(int a, int b, float w) {
  int delta = b - a;
  a = a + int(delta * w);
  return a;
}

void uderz(float fq, int numerFali) {
  env.setValue(0);
  wp.setBuffer(fale[numerFali]);
  wp.setFrequency(fq);
  env.addSegment(1, 0);
  env.addSegment(0, 1000);
}

void oscEvent(OscMessage komunikat) {
  if (komunikat.checkAddrPattern("/uderzenie")==true) {
    if (komunikat.checkTypetag("ii")) {
      int fq = komunikat.get(0).intValue();
      float fq2 = akord[fq];
      int fala = komunikat.get(1).intValue();
      uderz(fq2,fala);
      bcolor = random(255);
      newR = 100 + (int)random(300);
    }
  }
}

