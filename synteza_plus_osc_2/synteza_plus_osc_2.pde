import oscP5.*;
import netP5.*;
import beads.*;

AudioContext ac;
Gain g;
Envelope env;
WavePlayer wp;
Buffer[] fale = {
  Buffer.SINE, Buffer.SQUARE, Buffer.SAW, Buffer.TRIANGLE};

OscP5 oscP5;
NetAddress instr;
//NetAddress[] instr = {
//  new NetAddress("192.168.24.41",12000),
//  new NetAddress("192.168.24.41",12000),
//  new NetAddress("192.168.24.41",12000),
//  new NetAddress("192.168.24.41",12000),
//};

float[] akord  = {
  60, 64, 67, 69
};

float bcolor = 0;
int r = 300;
int newR = 0;

void setup() {
  size(400, 400);

  ac = new AudioContext(); 
  for (int i = 0; i<akord.length; i++) {
    akord[i] = Pitch.mtof(akord[i]);
    //   println(akord[i]);
  }

  env= new Envelope(ac, 0);
  g = new Gain(ac, 1, env);
  wp = new WavePlayer(ac, akord[(int)random(4)], Buffer.SINE);
  ///////////////////////////////////////////
  g.addInput(wp);
  ac.out.addInput(g);
  ac.start();

  oscP5 = new OscP5(this, 12000);
}

void draw() {
  background(bcolor);
//   r = env.getCurrentValue()*200;
  r = easing(r, newR, 0.5);
  if(r == newR) newR = 0;
  ellipse(200,200, r,r);
}

void keyReleased() {
  OscMessage komunikat = new OscMessage("/uderzenie");
  float fq = akord[(int)random(akord.length)];
  komunikat.add(fq);
  int numerFali = (int)random(fale.length);
  komunikat.add(numerFali);
  String ip = "192.168.24." + str((int)random(41,42));
  instr = new NetAddress("localhost",12000);
  oscP5.send(komunikat, instr);
}

void oscEvent(OscMessage komunikat) {
  if (komunikat.checkAddrPattern("/uderzenie")==true) {
    if (komunikat.checkTypetag("fi")) {
      float fq = komunikat.get(0).floatValue();
      int fala = komunikat.get(1).intValue();
      uderz(fq,fala);
      bcolor = random(255);
      newR = 100 + (int)random(300);
    }
  }
}

void uderz(float fq, int numerFali) {
  env.setValue(0);
  wp.setBuffer(fale[numerFali]);
  wp.setFrequency(fq);
  env.addSegment(1, 0);
  env.addSegment(0, 1000);
}

int easing(int a, int b, float w){
  int delta = b - a;
  a = a + int(delta * w);
  return a;
}

